#===============================================================================
#USAGE : ./ssh-scp.sh hostname ipaddress password
#DESCRIPTION : Establishes ssh connection between two computer and monitor the packages. 
#PARAMS: Pass as arguments the hostname, ipaddress and password.
#TIPS: Make it simple, no need to overcomplicate it.
#===============================================================================
#
#!/bin/bash
#START
sshpass -p $3 ssh $1@$2<< EOF
echo "spacebar" | sudo -S timeout 10 tcpdump -G 10 -i enp0s3 port 22 -w ssh-ping-packets.pcap &
timeout 12 ping 10.0.2.4
sshpass -p 'spacebar' scp ssh-ping-packets.pcap mithil@10.0.2.4:~
EOF
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#===============================================================================
#USAGE : ./curl.sh
#DESCRIPTION : A menu to choose between making a http request or download a file using curl. Also to check if domain is up
#PARAMS: No params 
#TIPS: Instead of params, read user input
#===============================================================================
#
#!/bin/bash
#START
#------Delete this line and write your code here between START and END.
echo "(1) Make GET request and receive a JSON"
echo "(2) Download a file from a remote server"
echo ""

set -e
#isUp= $(curl -Is "$1" | head -n 1 | grep -Eo '[0-9]{2,4}')
#echo "$isUp"
	if [ "$(curl -Is "$1" | head -n 1 | grep -Eo '[0-9]{2,4}')" = "" ] || [ "$(curl -Is "$1" | head -n 1 | grep -Eo '[0-9]{2,4}')" != "200" ]; then
		#	echo "invalid"
		echo "invalid URL"
		echo $(curl -Is "$1" | head -n 1 | grep -Eo '[0-9]{2,4}')
		exit
	fi
echo "valid URL"
echo "Enter the operation: "
echo "$1"
	
read op
if [ "$op" -eq 1 ]; then
	echo "here"
	curl -X GET "$1" -H "Accept: application/json"> ~/Desktop/lg-bootcamp/ms2/T3/get-json-response.json
fi
if [ "$op" -eq 2 ]; then
	curl $1 -o $2
	file= "~/$2"
	echo "$file"
	if [[ $file =~ \.zip$ ]]; then
		echo "zipped file" 
		unzip "$file"
	fi
fi


	
	


#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

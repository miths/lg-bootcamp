#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS: 
#TIPSadasd
#===============================================================================
#sdad
#!/bin/bash
#START
#------Delete this line and write your code here between START and END.
echo "(1) Relaunch"
echo "(2) Restart"
echo "(3) Tilt Screen"
echo "(4) Change Keyboard language"
echo ""
echo "Enter the operation: "
read op
if [ "$op" -eq 1 ]; then
    echo "shutting down..."
	sleep 1 
    sudo shutdown -h now
fi
if [ "$op" -eq 2 ]; then
    COUNTER=5
        while [  $COUNTER -gt 0 ]; do
            echo resterting in $COUNTER
	    sleep 1 
            let COUNTER=COUNTER-1
        done
    echo "restarting now"
    shutdown -r now
fi


if [ "$op" -eq 3 ]; then
echo "side: "
    read side
	echo $side
    if [ "$side"="left" ]; then
    	xrandr -o left
    fi
    if [ "$side"= "right" ]; then
        xrandr -o right
    fi
    if [ "$side" = "normal" ]; then
        xrandr -o normal
    fi
fi


if [ "$op" -eq 4 ]; then
echo "language: "
    read lang
    setxkbmap $lang
fi

	

#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
